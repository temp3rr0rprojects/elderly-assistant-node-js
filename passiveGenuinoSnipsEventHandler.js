const { Client } = require('pg')
const connectionString = 'postgresql://pi:ma121284@127.0.0.1:5432/genuino101'

var request = require('request');
var nodemailer = require('nodemailer');
var mqtt = require('mqtt');
var mqtt_client = mqtt.connect('mqtt://localhost:9898');
var noble = require('noble');

var verbose = true;
var fell = false;
var sentMail = false;
var lastStepsCount = 0;
var smokeDetected = false;

// Search only for the Service UUID of the device (remove dashes) 
var serviceUuids = ['181f']; 
// Search only for the led charateristic 
var characteristicUuids = ['3a19']; 
// start scanning when bluetooth is powered on 
noble.on('stateChange', function(state) { 
 if (state === 'poweredOn') { 
   noble.startScanning(serviceUuids); 
 } else { 
   noble.stopScanning(); 
 } 
}); 

var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'elderlyassistanttest@gmail.com',
    pass: '181f3a199898'
  }
});

var mailOptions = {
  from: 'elderlyassistanttest@gmail.com',
  to: 'madks@hotmail.com',
  subject: 'Help is needed.',
  text: 'Hello family members. An automated system detected that I need help.'
};

// Search for BLE peripherals 
noble.on('discover', function(peripheral) { 
 peripheral.connect(function(error) { 
   if (verbose)
     console.log('connected to peripheral: ' + peripheral.uuid); 

   // Only discover the service we specified above 
   peripheral.discoverServices(serviceUuids, function(error, services) { 
     var service = services[0]; 
     console.log('discovered service'); 
     service.discoverCharacteristics(characteristicUuids, function(error, characteristics) { 
       console.log('discovered characteristics'); 
       // Assign Characteristic 
       var ledCharacteristic = characteristics[0]; 
       setInterval(function() { 
           ledCharacteristic.read(function(error, data) { 
           // data is a buffer 
	   if (verbose)
            console.log('sensor values: ' + data.readUInt8(0) + ', ' + data.readUInt8(1) + ', ' + data.readUInt8(2) + ', '+ data.readUInt8(3) + ', '+ data.readUInt8(4) + ', '+ data.readUInt8(5));

           var fell = data.readUInt8(0) === 2 ? true : false;
	   var stepCount = data.readUInt8(1);
	   if (stepCount !==0) {
		if (stepCount != lastStepsCount) {
			lastStepsCount = stepCount;
			
			const client = new Client({
  				connectionString: connectionString,
			})
			client.connect()
			
			client.query('INSERT INTO steps(ts,stepcount) VALUES (CURRENT_TIMESTAMP , ' + stepCount + ')', (err, res) => {
				if (verbose)
					console.log(err, res);
				client.end();
			})
		}	
	   }
	   
           if (fell && sentMail === false) {
		if (verbose)
			console.log("Fell. Sending email. Steps: " + stepCount);		
		
		say("I have noticed you fell down. I am sending an email to your family, to warn them.");
		sentMail = true;

		mailOptions.subject += " I fell down.";
		mailOptions.text += " I fell down.";
		transporter.sendMail(mailOptions, function(error, info){
		  if (error) {
		    console.log(error);
		  } else {
		    console.log('Email sent: ' + info.response);
		  }
		});

	   }

	// TODO: Restfull call for the gas sensor. If > 0.75 * 1024, then alert Mail for help
	var mq2Value = 0;

	var url = 'http://192.168.0.139/variables/mq7';

	request.get({
	    url: url,
	    json: true,
	    headers: {'User-Agent': 'request'}
	  }, (err, res, data) => {
	    if (err) {
		if (verbose)
		      console.log('Error:', err);
	    } else if (res.statusCode !== 200) {
		if (verbose)
		      console.log('Status:', res.statusCode);
	    } else {
	      // data is already parsed as JSON:
		if (verbose)
		      console.log("Mq7 value: " + data.mq7);
		mq2Value = data.mq7;

		if (mq2Value >= (0.75 * 1024))
                	smokeDetected = true;
	        if (smokeDetected && sentMail === false) {
        	        if (verbose)
                	        console.log("Smoke detected. Sending email. MQ2 value: " + mq2Value);
	                say("I have noticed smoke in the house. I am sending an email to your family, to warn them.");

		      	mailOptions.subject += " Smoke detected in the house.";
                	mailOptions.text += " Smoke detected in the house.";			
			sentMail = true;
	                transporter.sendMail(mailOptions, function(error, info){
        	          if (error) {
	                    console.log(error);
	                  } else {
	                    console.log('Email sent: ' + info.response);
	                  }
                });

	        }
	    }
	});

               }); 
       }, 5000); // Every 5 seconds
     }); 
   }); 
 }); 
}); 


function say(text) {
    console.log(text);
    mqtt_client.publish('hermes/tts/say', JSON.stringify({'text': text}));
}
